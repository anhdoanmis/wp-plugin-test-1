<?php
/**
* Plugin Name: Comment Rating
* Plugin URI: https://www.wordpress.org/
* Description: This is my test.
* Version: 1.0
* Author: Ngoc Anh
* Author URI: https://www.wordpress.org/
**/

add_action( 'comment_form_logged_in_after', 'additional_fields' );
add_action( 'comment_form_after_fields', 'additional_fields' );

function additional_fields () {
  

  echo '<p class="comment-form-rating">'.
  '<label for="rating">'. __('Rating') . '<span class="required">*</span></label>
  <span class="commentratingbox">';

    for( $i=1; $i <= 5; $i++ )
    echo '<span class="commentrating"><input type="radio" name="rating" id="rating" value="'. $i .'" '.($i==3?" checked ":"").'/>'. $i .'</span>';

  echo'</span></p>';

}
add_action( 'comment_post', 'save_comment_meta_data' );
function save_comment_meta_data( $comment_id ) {   
  
    if ( ( isset( $_POST['rating'] ) ) && ( $_POST['rating'] != ’) )
    $rating = wp_filter_nohtml_kses($_POST['rating']);
    add_comment_meta( $comment_id, 'rating', $rating );
    $comment_api_url = "http://localhost:8000/api/addComment?post_id=".$_POST['comment_post_ID']."&post_title=".urlencode(get_the_title($_POST['comment_post_ID']))."&comment_id=".$comment_id."&rating=".$_POST['rating'];
    $json = file_get_contents($comment_api_url);

}
add_filter( 'preprocess_comment', 'verify_comment_meta_data' );
function verify_comment_meta_data( $commentdata ) {
    if ( ! isset( $_POST['rating'] ) )
        wp_die( __( 'Error: You did not add a rating. Hit the Back button on your Web browser and resubmit your comment with a rating.' ) );
    return $commentdata;
}
add_action('init','func_plugin_init');
function func_plugin_init(){
    add_shortcode('average_post_rating_list','func_average_post_rating_list');
}
function func_average_post_rating_list($args, $content){
    $comment_api_url = "http://localhost:8000/api/averageRating";
    $json = file_get_contents($comment_api_url);
    $average_post_rating_list = json_decode($json);
    $posts = get_posts(['post_type' => 'post', 'post_status' => 'publish', 'order' => 'ASC', 'order_by' => 'post_title']);
    ob_start();
    ?>
    <h2 class="h2--title"><?php echo __('Average Rating Of Each Post: ','twentynineteen');?></h2>
    <div class="" style="">
    <div class="col-md-9 col-sm-9 float-left"><?php echo __('Post Title','twentynineteen');?></div>
    <div class="col-md-3 col-sm-3 float-left"><?php echo __('Average Rating','twentynineteen');?></div>
    <?php
    foreach($posts as $pot):
    ?>
        <div class="col-md-9 col-sm-9 float-left"><?php echo $pot->post_title;?></div>
        <div class="col-md-3 col-sm-3 float-left"><?php echo searchForId($pot->ID,$average_post_rating_list);?></div>
    <?php
    endforeach;

    ?>
    </div>
<?php
    $result = ob_get_contents();
    ob_end_clean();

    return $result;
}
function searchForId($id, $array) {
    foreach ($array as $key => $val) {
        if ($val->post_id == $id) {
            return $val->average_rating;
        }
    }
    return null;
}